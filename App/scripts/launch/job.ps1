push-location $PSScriptRoot
$log_dir = "$PSScriptRoot\..\..\logs"
$log_file = "$log_dir\launch.log"

write-output "Launch ninja.exe" > $log_file
../../bin/ninja.exe --version >> $log_file
$exit_code = $LASTEXITCODE

more $log_file

exit $exit_code
