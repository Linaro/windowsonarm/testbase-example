# testbase-example

This repository creates an example package for Microsoft TestBase, uploading a
package for arm64 and x64.

Additional documentation can be found in ./Confluence-Microsoft TestBase.pdf,
which is a file available on Linaro Confluence.

Upload
------

See .gitlab-ci.yml for commands to call.

Upload is made using ./CreateOrUpdatePackage.ps1 script ([original
file](https://github.com/microsoft/testbase/blob/main/Utilities/CICD/Azure-DevOps/CreateOrUpdatePackage.ps1)).
We added instructions given by Microsoft to add windows-arm64 upload support.

Selection between x64 and arm64 runners is made by defining a specific
targetOSs in PackageSetting.json.

A single package can only targets x64 OR arm64, but not both at the same time.

Various env var must be defined to allow upload of a package:

- AZURE_CLIENT_ID: a specific Azure App that must be granted Contributor Role
  for the TestBase Account: [link](https://portal.azure.com/#view/Microsoft_AAD_RegisteredApps/ApplicationMenuBlade/~/Overview/appId/ddccb884-d52d-4d64-b74b-15ee8ce1e8a8/isMSAApp~/false)
- AZURE_TENANT_ID: defined by Azure Directory for Linaro [link](https://portal.azure.com/#view/Microsoft_AAD_IAM/ActiveDirectoryMenuBlade/~/Overview)
- AZURE_SUBSCRIPTION_ID: subscription for project [link](https://portal.azure.com/#@linaro.org/resource/subscriptions/55247270-80a1-4f87-b5d4-f94d773d9371/overview)
- AZURE_CLIENT_SECRET: a secret attached to Azure App referenced by
  AZURE_CLIENT_ID. This token expires every two years, and can be renewed
  [here](https://portal.azure.com/#view/Microsoft_AAD_RegisteredApps/ApplicationMenuBlade/~/Credentials/appId/ddccb884-d52d-4d64-b74b-15ee8ce1e8a8/isMSAApp~/false)
- RESOURCE_GROUP_NAME: Azure group holding resources. WoA in our case. [link](https://portal.azure.com/#@linaro.org/resource/subscriptions/55247270-80a1-4f87-b5d4-f94d773d9371/resourceGroups/WoA/overview)
- TESTBASE_ACCOUNT_NAME: TestBase account. WoA in out case. [link](https://portal.azure.com/#@linaro.org/resource/subscriptions/55247270-80a1-4f87-b5d4-f94d773d9371/resourceGroups/WoA/providers/Microsoft.TestBase/testBaseAccounts/WoA/overview)

WindowsOnArm Gitlab is already setup so any repository has access to those
variables.

Configure account
-----------------

Ask to Linaro IT to create an Azure account, and add you to WoA/WoA TestBase
Account with a "Contributor" role.

Access packages
---------------

Once logged in using your Linaro account, list of packages is available using
this [link](https://portal.azure.com/?feature.packageArm64=true#@linaro.org/resource/subscriptions/55247270-80a1-4f87-b5d4-f94d773d9371/resourceGroups/WoA/providers/Microsoft.TestBase/testBaseAccounts/WoA/manage_apps).

Note: This link contains "?feature.packageArm64=true" flag, which is needed to
see jobs running on arm64 runners.

Receive packages status emails
------------------------------

To get notified by emails about new failures, you need to edit settings [here](https://portal.azure.com/#@linaro.org/resource/subscriptions/55247270-80a1-4f87-b5d4-f94d773d9371/resourceGroups/WoA/providers/Microsoft.TestBase/testBaseAccounts/WoA/email_notification).

FAQ
---

- When pushing a new version, pipeline fails with a message error "Failed to
  update package". How to solve this?

A package with the same name and version is currently being tested. TestBase
does not allow to push a new version of it during this. Thus, you need to delete
it manually (from this [link](https://portal.azure.com/?feature.packageArm64=true#@linaro.org/resource/subscriptions/55247270-80a1-4f87-b5d4-f94d773d9371/resourceGroups/WoA/providers/Microsoft.TestBase/testBaseAccounts/WoA/manage_apps)).
